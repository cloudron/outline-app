FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

ARG NODE_VERSION=20.18.3
RUN mkdir -p /usr/local/node-${NODE_VERSION} && curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

# renovate: datasource=github-releases depName=outline/outline versioning=semver extractVersion=^v(?<version>.+)$
ARG OUTLINE_VERSION=0.82.0

RUN curl -L https://github.com/outline/outline/archive/refs/tags/v${OUTLINE_VERSION}.tar.gz | tar -xzf - -C /app/code --strip-components=1

# Install and build the outline project
RUN yarn install --frozen-lockfile --no-cache && \
    yarn build && \
    yarn cache clean

COPY cron.sh env.sh.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
