#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until, Key } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;
    const DOCUMENT_ID = Math.floor((Math.random() * 100) + 1);
    const DOCUMENT_TITLE = 'Test document ' + DOCUMENT_ID;

    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;

    let browser, app, cloudronName;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');

        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true, waitForButton = false) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        if (waitForButton) {
            await waitForElement(By.xpath(`//button[contains(., "${cloudronName}")]`));
            await browser.findElement(By.xpath(`//button[contains(., "${cloudronName}")]`)).click();
        }

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.xpath('//div[text()="Home"]'));
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn + '/home');
        await browser.sleep(2000);
        await waitForElement(By.xpath('//div[text()="Home"]'));
    }

    async function getLoginPage() {
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);
        await waitForElement(By.xpath('//h2[contains(text(), "Login")]'));
    }

    async function createDocument() {
        await browser.get('https://' + app.fqdn + '/home');
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//a[contains(@href, "/collection/welcome")]')).click();
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//a[contains(., "New doc")]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//span[@data-placeholder="Untitled"]'));

        await browser.findElement(By.xpath('//span[@data-placeholder="Untitled"]')).click();
        await browser.findElement(By.xpath('//span[@data-placeholder="Untitled"]')).sendKeys(DOCUMENT_TITLE);
        await browser.sleep(5000);

        await browser.get('https://' + app.fqdn + '/home');
        await browser.sleep(2000);

        await waitForElement(By.xpath('//h3//span[contains(., "' + DOCUMENT_TITLE  + '")]'));
    }

    async function checkDocument() {
        await browser.get('https://' + app.fqdn + '/home');
        await browser.sleep(2000);

        await waitForElement(By.xpath('//h3//span[contains(., "' + DOCUMENT_TITLE  + '")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // just install. nothing to test because one cannot login without sso. even email login won't work without sso
    // https://github.com/outline/outline/discussions/2882
    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can get the login page', getLoginPage);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // sso
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', loginOIDC.bind(null, USERNAME, PASSWORD, false, false /* click button */));
    it('can get the main page', getMainPage);
    it('can create Document', createDocument);

    it('can restart app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    });

    it('can get the main page', getMainPage);
    it('check Document', checkDocument);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can get the main page', getMainPage);
    it('check Document', checkDocument);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login', loginOIDC.bind(null, USERNAME, PASSWORD, true, true /* click button */));
    it('can get the main page', getMainPage);
    it('check Document', checkDocument);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id com.getoutline.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', loginOIDC.bind(null, USERNAME, PASSWORD, true, false));
    it('can get the main page', getMainPage);
    it('can create Document', createDocument);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get the main page', getMainPage);
    it('check Document', checkDocument);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

