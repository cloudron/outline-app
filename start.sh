#!/bin/bash

set -eu

mkdir -p /app/data/storage

if [[ ! -f /app/data/env.sh ]]; then
    cp /app/pkg/env.sh.template /app/data/env.sh
    secret_key=$(openssl rand -hex 32)
    sed -e "s/SECRET_KEY=.*/SECRET_KEY=${secret_key}/g" -i /app/data/env.sh
    utils_secret=$(openssl rand -hex 32)
    sed -e "s/UTILS_SECRET=.*/UTILS_SECRET=${utils_secret}/g" -i /app/data/env.sh
fi

source /app/data/env.sh

export NODE_ENV=production
export DATABASE_URL=$CLOUDRON_POSTGRESQL_URL
export DATABASE_CONNECTION_POOL_MIN=5
export DATABASE_CONNECTION_POOL_MAX=20
export PGSSLMODE=disable
export REDIS_URL="redis://default:$CLOUDRON_REDIS_PASSWORD@$CLOUDRON_REDIS_HOST:$CLOUDRON_REDIS_PORT"

export URL=$CLOUDRON_APP_ORIGIN
export PORT=3000

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    export OIDC_CLIENT_ID=$CLOUDRON_OIDC_CLIENT_ID
    export OIDC_CLIENT_SECRET=$CLOUDRON_OIDC_CLIENT_SECRET
    export OIDC_AUTH_URI=$CLOUDRON_OIDC_AUTH_ENDPOINT
    export OIDC_TOKEN_URI=$CLOUDRON_OIDC_TOKEN_ENDPOINT
    export OIDC_USERINFO_URI=$CLOUDRON_OIDC_PROFILE_ENDPOINT
    export OIDC_USERNAME_CLAIM=preferred_username
    export OIDC_DISPLAY_NAME=${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}
    export OIDC_SCOPES="openid profile email"
fi

export FORCE_HTTPS=false
export ENABLE_UPDATES=false # Does not work with the read-only filesystem
export LOG_LEVEL=info
export SMTP_HOST=$CLOUDRON_MAIL_SMTP_SERVER
export SMTP_PORT=$CLOUDRON_MAIL_SMTP_PORT
export SMTP_USERNAME=$CLOUDRON_MAIL_SMTP_USERNAME
export SMTP_PASSWORD=$CLOUDRON_MAIL_SMTP_PASSWORD
export SMTP_FROM_EMAIL="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Outline} <${CLOUDRON_MAIL_FROM}>"
export SMTP_REPLY_EMAIL=$CLOUDRON_MAIL_FROM
export SMTP_SECURE=false

echo "=> Changing ownership"
chown -R cloudron:cloudron /app/data

echo "=> Running migration"
gosu cloudron:cloudron yarn db:migrate --env=production-ssl-disabled

echo "=> Starting outline"
exec /usr/local/bin/gosu cloudron:cloudron yarn start
