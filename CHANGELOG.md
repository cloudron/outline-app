[0.1.0]
* Initial version

[0.2.0]
* Fixes to manifest
* Redis can use auth
* Fix email format

[0.3.0]
* Internal build changes

[0.4.0]
* admin note

[0.5.0]
* add schedule job

[0.6.0]
* Update Outline to 0.73.0
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.73.0)
* Bulk import was re-architected to support imports larger than 2Gb in size
* Added "Copy document" dialog that allows choosing whether to duplicate nested documents in #6009
* Todo items in editor can now be toggled with Cmd+Enter (Ctrl+Enter on Windows)
* Added a button on comments to upload images in #6092
* Documents pinned to a collection are now shown above the description
* "Full width" document preference is now remembered per-user
* New style and polish for toast messages in #6053

[0.7.0]
* Optional SSO support

[0.8.0]
* Update Outline to 0.74.0
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.74.0)
* Clicking on a link while editing will now automatically select the entire link
* Mermaid diagrams are now rendered in HTML exports in #6205
* Added "Share" option to document menu on mobile
* New translations
* Updated to Node 20 LTS
* [API] comments.list endpoint can now be used without a document ID
* Find and replace dialog no longer gets stuck without replace button
* Fixed corruption in imports in #6162
* Fixed recurring OOM crash when DataDog and Sentry are enabled.
* Fixed display of video nodes in document history
* Fixed members can now create templates again
* Fixed viewing public shares failed if user has an expired token in #6134
* Restored ability to comment in code blocks in #6154
* Comment functionality should not show in toolbar for view-only users in #6011
* Added support for tldraw snapshot links in #6210
* Restored PWA install in recent Chrome versions
* Fixed extra empty page in HTML exports in #6205
* Emoji in template titles are now applied correctly in #6169
* Many dependency updates

[1.0.0]
* Update Outline to 0.75.0
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.75.0)

[1.0.1]
* Update Outline to 0.75.1
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.75.1)
* Added flag --no-migrate to optionally suppress automatic migrations on startup
* Added option `OIDC_LOGOUT_URI` to trigger logout in OIDC on logout in Outline by @Shuttleu in https://github.com/outline/outline/pull/6539
* Added option `OIDC_DISABLE_REDIRECT` to prevent automatic OIDC redirect by @Shuttleu in https://github.com/outline/outline/pull/6544
* `MAXIMUM_IMPORT_SIZE` was deprecated and split into `FILE_STORAGE_IMPORT_MAX_SIZE` (for documents) and `FILE_STORAGE_WORKSPACE_IMPORT_MAX_SIZE` (for workspace imports) in https://github.com/outline/outline/pull/6566
* Fixed new paragraphs are lost in table cells imported from HTML
* Fixed an issue where tables with empty cells could fail to import from HTML
* Fixed an issue where quoted search queries with colons could cause a server error
* Fixed regression in styling of search filter options
* Hide image drag handles, empty captions, and selected outlines when printing in https://github.com/outline/outline/pull/6550
* Missing highlighted context on search results page in https://github.com/outline/outline/pull/6549
* Error for self-host draw.io by @TaoChan1005 in https://github.com/outline/outline/pull/6554
* Propagate Enter key events in SuggestionsMenu allows pressing Enter after typed url in embed input by @superkelvint in https://github.com/outline/outline/pull/6556
* Do not load opensearch.xml from CDN, so it uses the correct subdomain by @waf in https://github.com/outline/outline/pull/6567
* CMD+Z now works now works when the editor component does not technically have focus
* Served HTML lang attribute now reflects installation default language

[1.0.2]
* Update Outline to 0.75.2
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.75.2)
* Regression in 0.75.1 that required `MAXIMUM_IMPORT_SIZE` config to be set
* Fixed an issue where the comment sidebar would overflow the screen at tablet size
* Removed the maximum of 10 allowed domains

[1.1.0]
* Update Outline to 0.76.0
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.76.0)
* Improved search matching for "phrases" and urls in #6800
* Added "Search in document" functionality to restrict a search to a subtree
* Improved the quality of snippets for search results in #6828
* Processing of documents.import now happens on worker
* Added support for emoji version 15.0
* Added 80+ additional collection icons in #6803
* Added "Archive all notifications" option @hmacr in #6599
* Added sorting controls to tables in #6678
* Added support for custom db schema by @axelrindle in #6670
* Default user language is now inferred when signing in via Google in #6679

[1.1.1]
* Update Outline to 0.76.1
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.76.1)
* When importing a HTML file with tables it would be imported as raw HTML if a cell contained a list or heading
* Fixed an issue that prevented unarchiving documents
* Fixed an issue that prevented connecting Slack channels to a collection

[1.2.0]
* Move secret keys into `/app/data/env.sh` from dot files
* Make attachment storage configurable

[1.3.0]
* Update Outline to 0.77.0
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.77.0)

[1.3.1]
* Update Outline to 0.77.1
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.77.1)
* Fixed an issue that could cause the app to become unresponsive when zooming out of an image
* Restored the text field in outgoing document webhooks
* Page scroll now resets correctly on mobile when navigating between documents
* Tables with a single remaining column now automatically fill available width

[1.3.2]
* Update Outline to 0.77.2
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.77.2)

[1.3.3]
* Update Outline to 0.77.3
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.77.3)

[1.4.0]
* Update Outline to 0.78.0
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.78.0)

[1.5.0]
* Update Outline to 0.79.0
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.79.0)
* Add support to embed any webpage with /embed command by @Blendman974 in #7319
* Add support for Dropbox embed by @LouisKottmann in #7299
* Add support for workspace templates by @hmacr in #7150
* Added R language highlighting in code blocks #7473
* New and improved translations
* Shared and read-only documents can now be dragged to "Starred" in #7506
* Outgoing emails now include headers to enhance threading in inbox by @hmacr in #7477
* Inserting /time and /datetime in templates will now insert a template variable

[1.5.1]
* Update Outline to 0.79.1
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.79.1)

[1.6.0]
* Update Outline to 0.80.0
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.80.0)
* Added a workspace preference for admins to disable individual user account removal
* New Umami analytics integration by @QYG2297248353 in #7366
* It is now possible to @mention users that do not have access to a document (they won't be notified) in #7601
* Behavior of Tab and Shift-Tab is now more natural in code blocks #7622
* "Public access" section of share dialog is now always visible in #7617
* Archiving a document now includes a confirmation dialog
* CMD-k with the cursor within an editor link now focuses the link field
* Home, Search, and Drafts are now sticky in the sidebar
* CMD-Shift-c is now a shortcut for inline code

[1.6.1]
* Update Outline to 0.80.1
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.80.1)
* Fixed release version
* Fixed serialization issue #7634

[1.6.2]
* Update Outline to 0.80.2
* [Full changelog](https://github.com/outline/outline/releases/tag/v0.80.2)

[1.7.0]
* Update outline to 0.81.0
* [Full Changelog](https://github.com/outline/outline/releases/tag/v0.81.0)
* It is now possible to emoji react to comments
* You can now archive an entire collection (https://github.com/outline/outline/issues/3141)
* Comments sidebar can now be sorted by position in document (https://github.com/outline/outline/issues/7725)
* `@mention` users can now be searched by email (https://github.com/outline/outline/issues/7679)
* There is now a "Leave document" menu item on documents that have been shared with you
* Publicly shared docs now have the option to disable search engine indexing (https://github.com/outline/outline/issues/7486)
* Import of CSV files is now supported, they will be converted to a document with a single table
* Tables now have an "Export as CSV" option (https://github.com/outline/outline/issues/7743)
* It is now possible to disable "Smart text" replacements with a user preference (https://github.com/outline/outline/issues/7769)
* Dark mode colors are now more consistent

[1.7.1]
* Fix invite email sending

[1.7.2]
* Update outline to 0.81.1
* [Full Changelog](https://github.com/outline/outline/releases/tag/v0.81.1)
* Fixed emails should not attempt to be sent if no SMTP authentication is configured
* Fixed cannot sort by "role" in members table in settings  https://github.com/outline/outline/issues/7986
* Further improvements to diacritics matching in `CMD`+`F`
* From address is no longer overridden in self-hosted installs https://github.com/outline/outline/issues/7929

[1.8.0]
* Add checklist

[1.8.1]
* Update outline to 0.82.0
* [Full Changelog](https://github.com/outline/outline/releases/tag/v0.82.0)
* Mention documents with `@`  document mentions will automatically keep title and icon in sync ([changelog](https://www.getoutline.com/changelog/document-mentions))
* Editor embeds are now vertically resizable
* When pasting a link there is now an *option* to embed or link
* Upgraded Mermaid diagrams to v11 ([#&#8203;7964](https://github.com/outline/outline/issues/7964))
* Copying a document now offers the choice of destination ([#&#8203;8030](https://github.com/outline/outline/issues/8030))
* When pasting remote images, they will now be automatically uploaded to Outline storage ([#&#8203;8301](https://github.com/outline/outline/issues/8301)) ([#&#8203;8086](https://github.com/outline/outline/issues/8086))
* New tables in the editor now start with fixed width (but resizable) columns
* Email notifications are no longer sent for insignificant document changes
* Collection description was moved to it's own tab
* Copying text out of the editor now copies markdown to the clipboard where appropriate
* Added command to create nested document from command menu ([#&#8203;8365](https://github.com/outline/outline/issues/8365))
* Creating a new document from the sidebar is now much faster
* It is now possible to create API keys with scopes ([#&#8203;8297](https://github.com/outline/outline/issues/8297))
* Added new icons into picker
* ...

