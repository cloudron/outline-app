### About

The fastest knowledge base for growing teams. Beautiful, realtime collaborative, feature packed, and markdown compatible.

### Features

* Open source
* Translated in 17+ languages
* Built in public (see [Changelog](https://www.getoutline.com/changelog))
* Integrate and embed content from over [20 other tools](https://www.getoutline.com/integrations)
