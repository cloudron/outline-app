# https://docs.getoutline.com/s/hosting/doc/scheduled-jobs-RhZzCt770H

echo "=> Running scheduled daily job"

export UTILS_SECRET=$(cat /app/data/.utils_secret)
curl "http://localhost:3000/api/cron.daily?token=${UTILS_SECRET}"

